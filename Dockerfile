# Etape 1: Servir l'application avec Nginx
FROM nginx:alpine

# Copier les fichiers de construction depuis l'étape de construction vers le répertoire de travail de nginx
COPY /dist/boat-app-front-end /usr/share/nginx/html

# Exposer le port 80
EXPOSE 80

# Lancer nginx en mode foreground
CMD ["nginx", "-g", "daemon off;"]
