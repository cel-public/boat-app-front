import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {BoatListComponent} from './boat-list/boat-list.component';
import {authGuard} from "./auth.guard";
import {BoatEditComponent} from "./boat-edit/boat-edit.component";
import {BoatDetailsComponent} from "./boat-details/boat-details.component";

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'edit-boat/:id', component: BoatEditComponent},
  {path: 'edit-boat', component: BoatEditComponent},
  {path: 'boats', component: BoatListComponent, canActivate: [authGuard]},
  {path: 'boats/:id', component: BoatDetailsComponent},
  {path: '', redirectTo: '/boats', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
