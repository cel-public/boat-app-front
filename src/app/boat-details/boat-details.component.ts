import {Component} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {BoatsService} from "../boats.service";

@Component({
  selector: 'app-boat-details',
  templateUrl: './boat-details.component.html',
  styleUrls: ['./boat-details.component.css']
})
export class BoatDetailsComponent {
  boat: any;

  constructor(private route: ActivatedRoute, private boatsService: BoatsService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const boatId = params['id'];
      this.loadBoatDetails(boatId);
    });
  }

  private loadBoatDetails(boatId: string): void {
    this.boatsService.getBoatById(boatId).subscribe({
      next: (boat) => this.boat = boat,
      error: (error) => console.error('Erreur lors du chargement des données du bateau:', error)
    });
  }
}
