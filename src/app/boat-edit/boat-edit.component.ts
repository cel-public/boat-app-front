import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BoatsService} from '../boats.service';

@Component({
  selector: 'app-boat-edit',
  templateUrl: './boat-edit.component.html',
  styleUrls: ['./boat-edit.component.css']
})
export class BoatEditComponent implements OnInit {
  boatId: string = '';
  isUpdateMode: boolean = false;
  boatData: any = {};

  constructor(private route: ActivatedRoute, private router: Router, private boatsService: BoatsService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.boatId = params['id'];
      if (this.boatId) {
        this.isUpdateMode = true;
        this.loadBoatData();
      }
    });
  }

  onSubmit(): void {
    if (this.isUpdateMode) {
      this.updateBoat();
    } else {
      this.createBoat();
    }
  }

  updateBoat(): void {
    this.boatsService.updateBoat(this.boatId, this.boatData).subscribe({
      next: () => this.router.navigate(['/boats']),
      error: (error) => console.error('Erreur lors de la mise é jour du bateau:', error)
    });
  }

  createBoat(): void {
    this.boatsService.createBoat(this.boatData).subscribe({
      next: () => this.router.navigate(['/boats']),
      error: (error) => console.error('Erreur lors de la création du bateau:', error)
    });
  }

  private loadBoatData(): void {
    this.boatsService.getBoatById(this.boatId).subscribe({
      next: (boat) => this.boatData = boat,
      error: (error) => console.error('Erreur lors du chargement des données du bateau:', error)
    });
  }
}
