import {CanActivateFn, Router} from '@angular/router';
import {inject} from "@angular/core";
import {AuthenticationService} from "./authentication.service";

export const authGuard: CanActivateFn = (route, state) => {
  if (inject(AuthenticationService).isLoggedIn()) {
    // L'utilisateur est connecté, autoriser la navigation
    return true;
  } else {
    // L'utilisateur n'est pas connecté, rediriger vers la page de connexion
    inject(Router).navigate(['/login']);
    return false;
  }
};
