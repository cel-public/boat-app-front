import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from "../authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMessage: string = '';

  constructor(private authService: AuthenticationService, private router: Router) {
  }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['/boats']);
    }
  }

  login(username: string, password: string): void {
    this.authService.login(username, password).subscribe({
      next: data => {
        console.log('Connexion réussie', data);
        this.router.navigate(['/boats']);
      },
      error: err => {
        console.error('Erreur de connexion', err);
        this.errorMessage = 'Nom d\'utilisateur ou mot de passe incorrect';
      }
    });
  }
}
