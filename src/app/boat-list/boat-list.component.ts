import {Component, OnInit} from '@angular/core';
import {BoatsService} from "../boats.service";
import {Router} from "@angular/router";
import {ConfirmationDialogComponent} from "../confirmation-dialog/confirmation-dialog.component";
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-boat-list',
  templateUrl: './boat-list.component.html',
  styleUrls: ['./boat-list.component.css'],
})
export class BoatListComponent implements OnInit {
  boats: any[] = [];

  constructor(private boatsService: BoatsService, private router: Router, private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.getBoats();
  }

  getBoats(): void {
    this.boatsService.getBoats().subscribe(
      (data) => {
        this.boats = data;
      },
      (error) => {
        console.error('Erreur lors de la récupération des bateaux', error);
      }
    );
  }

  createNewBoat() {
    this.router.navigate(['/edit-boat']);
  }

  editBoat(boatId: string): void {
    this.router.navigate(['/edit-boat', boatId]);
  }

  deleteBoat(boatId: string): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {message: 'Êtes-vous sûr de vouloir supprimer ce bateau?'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.boatsService.deleteBoat(boatId).subscribe({
          next: () => {
            this.getBoats();
          },
          error: (error) => console.error('Erreur lors de la suppression du bateau:', error)
        });
      }
    });
  }

  viewBoatDetails(boatId: string): void {
    this.router.navigate(['/boats', boatId]);
  }
}
