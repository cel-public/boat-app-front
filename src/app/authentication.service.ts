import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {BehaviorSubject, Observable, tap} from "rxjs";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public isAuthenticated$: Observable<boolean>;
  private apiUrl = environment.apiUrl + "/authentication";
  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  private isAuthenticatedSubject: BehaviorSubject<boolean>;
  private readonly AUTH_KEY = 'isAuthenticated';

  constructor(private http: HttpClient) {
    const storedAuth = localStorage.getItem(this.AUTH_KEY);
    this.isAuthenticatedSubject = new BehaviorSubject<boolean>(storedAuth === 'true');
    this.isAuthenticated$ = this.isAuthenticatedSubject.asObservable();
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post(this.apiUrl + '/login', {username, password}, this.httpOptions)
      .pipe(tap(() => {
        this.isAuthenticatedSubject.next(true);
        localStorage.setItem(this.AUTH_KEY, 'true');
      }));
  }

  logout(): Observable<any> {
    this.isAuthenticatedSubject.next(false);
    localStorage.removeItem(this.AUTH_KEY);
    return this.http.post(this.apiUrl + '/logout', {}, this.httpOptions);
  }

  isLoggedIn(): boolean {
    return this.isAuthenticatedSubject.value;
  }
}
