import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BoatsService {

  private apiUrl = environment.apiUrl + "/boats";

  private httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  getBoats(): Observable<any> {
    return this.http.get(`${this.apiUrl}`, this.httpOptions);
  }

  createBoat(boatData: any): Observable<any> {
    return this.http.post(`${this.apiUrl}`, boatData, this.httpOptions);
  }

  getBoatById(id: string): Observable<any> {
    return this.http.get(`${this.apiUrl}/${id}`);
  }

  updateBoat(id: string, boatData: any): Observable<any> {
    return this.http.put(`${this.apiUrl}/${id}`, boatData);
  }

  deleteBoat(boatId: string): Observable<any> {
    return this.http.delete(`${this.apiUrl}/${boatId}`);
  }
}
