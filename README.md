# Boat App Frontend

Bienvenue dans la documentation du frontend de l'application Boat App.
Ce frontend est développé en utilisant Angular version 16.2.10.

L'application permet aux utilisateurs connectés de gérer une liste de bateaux.

Un utilisateur standard peut ajouter, visualiser, modifier et supprimer uniquement les bateaux qui lui appartiennent.

D'autre part, un utilisateur admin a la même capacité, mais il peut également effectuer ces actions sur l'ensemble des bateaux, indépendamment du propriétaire.

## Prérequis

Avant de commencer, assurez-vous d'avoir installé les éléments suivants sur votre système :

- Node 20
- Angular 16

## Configuration

1. Clonez ce dépôt sur votre machine locale :

    ```bash
    git clone https://gitlab.com/chafikel/boatappfront.git
    ```

2. Naviguez vers le répertoire du projet :

    ```bash
    cd boatappfront
    ```

3. Installez les dépendances du projet :

    ```bash
    npm install
    ```

## Lancement de l'Application

1. Exécutez l'application en mode développement avec la commande suivante :

    ```bash
    ng serve
    ```

2. L'application sera disponible à l'adresse [http://localhost:4200](http://localhost:4200).

## CI/CD

L'application est automatiquement déployée sur AWS S3 via des pipelines CI/CD.
Vous pouvez accéder à la version déployée à l'adresse suivante : [http://boat-app-front.chafikel.fr](http://boat-app-front.chafikel.fr).

## Données de tests

### Utilisateurs

1. **Username:** user, **Email:** user@ptc.com, **Rôle:** ROLE_USER
2. **Username:** user2, **Email:** user2@ptc.com, **Rôle:** ROLE_USER
3. **Username:** admin, **Email:** admin@ptc.com, **Rôle:** ROLE_ADMIN

Ils ont tous le même mot de passe : **password**

### Bateaux

1. **Étoile Marine** appartient à **user**
2. **Aventure Aquatique** appartient à **user**
3. **Brisant des Vagues** appartient à **user**


4. **Mystère Marin** appartient à **user2**
5. **Horizon Infini** appartient à **user2**
6. **Perle des Mers** appartient à **user2**


7. **Tempête Silencieuse** appartient à **admin**

